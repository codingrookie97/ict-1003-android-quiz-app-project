package com.example.user.dcq;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by User on 7/5/2017.
 */

public class SingaporeQuestionFive extends AppCompatActivity {
    private Button btnA, btnB, btnC, btnD;
    private CountDownTimer countDownTimer;
    private FloatingActionButton btnBack;
    private int yourCurrentScore, attemptNo;
    private long timeLeft;
    private NotificationManager manager;
    private String name;
    private SweetAlertDialog warningA, warningB, warningC, warningD, warningQuit, progressA, progressB, progressC, progressD, wrongAns, correctAns, failedAns;
    private TextView txtScore, txtTimer, txtCurrentScoreTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.singaporeq5);
        btnA = (Button) findViewById(R.id.btnA);
        btnB = (Button) findViewById(R.id.btnB);
        btnC = (Button) findViewById(R.id.btnC);
        btnD = (Button) findViewById(R.id.btnD);
        btnBack = (FloatingActionButton) findViewById(R.id.btnBack);
        txtScore = (TextView) findViewById(R.id.txtScore);
        txtTimer = (TextView) findViewById(R.id.txtTimer);
        txtCurrentScoreTitle = (TextView) findViewById(R.id.txtCurrentScoreTitle);
        Intent intent = getIntent();
        yourCurrentScore = intent.getExtras().getInt("Q4Score");
        name = intent.getExtras().getString("Name");
        attemptNo = 1;
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        btnA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningADialog();
                timerPause();
            }
        });
        btnB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningBDialog();
                timerPause();
            }
        });
        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningCDialog();
                timerPause();
            }
        });
        btnD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningDDialog();
                timerPause();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningBackDialog();
                timerPause();
            }
        });
        txtCurrentScoreTitle.setText(name + "'s Current Score:");
        txtScore.setText(yourCurrentScore + " / 10");
        startTimer(30000);
    }

    public void startTimer(final long timeLengthMilli) {
        countDownTimer = new CountDownTimer(timeLengthMilli, 1000) {
            @Override
            public void onTick(long l) {
                timeLeft = l;
                txtTimer.setText(l/1000 + "s");
            }

            @Override
            public void onFinish() {
                txtTimer.setText("0s");
                showFailedToAnsPopup();
                playTimesUpNotification();
            }
        }.start();
    }

    public void timerPause() {
        countDownTimer.cancel();
    }

    public void timerResume() {
        startTimer(timeLeft);
    }

    public void showWarningADialog() {
        warningA = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningA.setTitleText("Are you sure?");
        warningA.setContentText("Is that your final answer?");
        warningA.setCancelable(false);
        warningA.setCanceledOnTouchOutside(false);
        warningA.setConfirmText("YES");
        warningA.show();
        warningA.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                showProgressADialog();
                sweetAlertDialog.dismiss();
            }
        });
        warningA.setCancelText("NO");
        warningA.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                timerResume();
            }
        });
    }

    public void showWarningBDialog() {
        warningB = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningB.setTitleText("Are you sure?");
        warningB.setContentText("Is that your final answer?");
        warningB.setCancelable(false);
        warningB.setCanceledOnTouchOutside(false);
        warningB.setConfirmText("YES");
        warningB.show();
        warningB.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                showProgressBDialog();
                sweetAlertDialog.dismiss();
            }
        });
        warningB.setCancelText("NO");
        warningB.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                timerResume();
            }
        });
    }

    public void showWarningCDialog() {
        warningC = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningC.setTitleText("Are you sure?");
        warningC.setContentText("Is that your final answer?");
        warningC.setCancelable(false);
        warningC.setCanceledOnTouchOutside(false);
        warningC.setConfirmText("YES");
        warningC.show();
        warningC.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                showProgressCDialog();
                sweetAlertDialog.dismiss();
            }
        });
        warningC.setCancelText("NO");
        warningC.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                timerResume();
            }
        });
    }

    public void showWarningDDialog() {
        warningD = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningD.setTitleText("Are you sure?");
        warningD.setContentText("Is that your final answer?");
        warningD.setCancelable(false);
        warningD.setCanceledOnTouchOutside(false);
        warningD.setConfirmText("YES");
        warningD.show();
        warningD.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                showProgressDDialog();
                sweetAlertDialog.dismiss();
            }
        });
        warningD.setCancelText("NO");
        warningD.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                timerResume();
            }
        });
    }

    public void onBackPressed() {
        showWarningBackDialog();
        timerPause();
    }

    public void showWarningBackDialog() {
        warningQuit = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningQuit.setTitleText("Return to main page?");
        warningQuit.setContentText("Are you sure you want to quit? Your score will be reset!");
        warningQuit.setCancelable(false);
        warningQuit.setCanceledOnTouchOutside(false);
        warningQuit.setConfirmText("YES");
        warningQuit.show();
        warningQuit.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Intent mainPage = new Intent(getApplicationContext(), MainPage.class);
                startActivity(mainPage);
                sweetAlertDialog.dismiss();
                manager.cancelAll();
                countDownTimer = null;
                finish();
            }
        });
        warningQuit.setCancelText("NO");
        warningQuit.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                timerResume();
            }
        });
    }

    public void showProgressADialog() {
        progressA = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressA.setTitleText("Confirming your answer");
        progressA.setContentText("Please wait!");
        progressA.setCancelable(false);
        progressA.setCanceledOnTouchOutside(false);
        progressA.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (attemptNo == 1) {
                    attemptNo++;
                    progressA.dismiss();
                    showWrongAnsPopupAttempt1();
                    playWrongNotificationAttempt1();
                    btnA.setVisibility(View.INVISIBLE);
                    btnA.setClickable(false);
                }
                else if (attemptNo > 1) {
                    progressA.dismiss();
                    showWrongAnsPopupAttempt2();
                    playWrongNotificationAttempt2();
                }
            }
        }, 500);
    }

    public void showProgressBDialog() {
        progressB = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressB.setTitleText("Confirming your answer");
        progressB.setContentText("Please wait!");
        progressB.setCancelable(false);
        progressB.setCanceledOnTouchOutside(false);
        progressB.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (attemptNo == 1) {
                    attemptNo++;
                    progressB.dismiss();
                    showWrongAnsPopupAttempt1();
                    playWrongNotificationAttempt1();
                    btnB.setVisibility(View.INVISIBLE);
                    btnB.setClickable(false);
                }
                else if (attemptNo > 1) {
                    progressB.dismiss();
                    showWrongAnsPopupAttempt2();
                    playWrongNotificationAttempt2();
                }
            }
        }, 500);
    }

    public void showProgressCDialog() {
        progressC = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressC.setTitleText("Confirming your answer");
        progressC.setContentText("Please wait!");
        progressC.setCancelable(false);
        progressC.setCanceledOnTouchOutside(false);
        progressC.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (attemptNo == 1) {
                    attemptNo++;
                    progressC.dismiss();
                    showWrongAnsPopupAttempt1();
                    playWrongNotificationAttempt1();
                    btnC.setVisibility(View.INVISIBLE);
                    btnC.setClickable(false);
                }
                else if (attemptNo > 1) {
                    progressC.dismiss();
                    showWrongAnsPopupAttempt2();
                    playWrongNotificationAttempt2();
                }
            }
        }, 500);
    }

    public void showProgressDDialog() {
        progressD = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressD.setTitleText("Confirming your answer");
        progressD.setContentText("Please wait!");
        progressD.setCancelable(false);
        progressD.setCanceledOnTouchOutside(false);
        progressD.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (attemptNo == 1) {
                    progressD.dismiss();
                    showCorrectAnsPopup();
                    playCorrectNotificationAttempt1();
                    yourCurrentScore = yourCurrentScore + 2;
                }
                else if (attemptNo > 1) {
                    progressD.dismiss();
                    showCorrectAnsPopup();
                    playCorrectNotificationAttempt2();
                    yourCurrentScore = yourCurrentScore + 1;
                }
            }
        }, 500);
    }

    public void showWrongAnsPopupAttempt1() {
        wrongAns = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        wrongAns.setTitleText("Wrong answer");
        wrongAns.setContentText("Sorry but your answer is wrong, you have 1 more attempt!");
        wrongAns.setConfirmText("OK");
        wrongAns.setCancelable(false);
        wrongAns.setCanceledOnTouchOutside(false);
        wrongAns.show();
        wrongAns.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
                timerResume();
            }
        });
    }

    public void showWrongAnsPopupAttempt2() {
        wrongAns = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        wrongAns.setTitleText("Wrong answer");
        wrongAns.setContentText("Sorry but your answer is wrong!");
        wrongAns.setConfirmText("OK");
        wrongAns.setCancelable(false);
        wrongAns.setCanceledOnTouchOutside(false);
        wrongAns.show();
        wrongAns.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                displayInfo();
                sweetAlertDialog.dismiss();
            }
        });
    }

    public void showFailedToAnsPopup() {
        failedAns = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        failedAns.setTitleText("No more time left");
        failedAns.setContentText("Sorry, time's up!");
        failedAns.setConfirmText("OK");
        failedAns.setCancelable(false);
        failedAns.setCanceledOnTouchOutside(false);
        failedAns.show();
        failedAns.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                displayInfo();
                sweetAlertDialog.dismiss();
            }
        });
    }

    public void showCorrectAnsPopup() {
        correctAns = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
        correctAns.setTitleText("Correct answer");
        correctAns.setContentText("Your answer is correct!");
        correctAns.setConfirmText("OK");
        correctAns.setCancelable(false);
        correctAns.setCanceledOnTouchOutside(false);
        correctAns.show();
        correctAns.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                displayInfo();
                sweetAlertDialog.dismiss();
            }
        });
    }

    public void playWrongNotificationAttempt1() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.cross);
        mBuilder.setContentTitle("Sorry, but your answer for Q5 is wrong");
        mBuilder.setContentText("Try again!");
        Uri notiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notiSound);
        mBuilder.setVibrate(new long[] {50, 1000});
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(5, mBuilder.build());
    }

    public void playWrongNotificationAttempt2() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.cross);
        mBuilder.setContentTitle("Sorry, but your answer for Q5 is wrong");
        mBuilder.setContentText("The answer is D: Singapore Flyer, better luck next time!");
        Uri notiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notiSound);
        mBuilder.setVibrate(new long[] {50, 1000});
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(5, mBuilder.build());
    }

    public void playTimesUpNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.cross);
        mBuilder.setContentTitle("Sorry, but your time in answering Q5 is up!");
        mBuilder.setContentText("The answer is D: Singapore Flyer, better luck next time!");
        Uri notiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notiSound);
        mBuilder.setVibrate(new long[] {50, 1000});
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(5, mBuilder.build());
    }

    public void playCorrectNotificationAttempt1() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.tick);
        mBuilder.setContentTitle("Your answer for Q5 is correct");
        mBuilder.setContentText("You get 2 points!");
        Uri notiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notiSound);
        mBuilder.setVibrate(new long[] {50, 1000});
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(5, mBuilder.build());
    }

    public void playCorrectNotificationAttempt2() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.tick);
        mBuilder.setContentTitle("Your answer for Q5 is correct");
        mBuilder.setContentText("You get 1 point!");
        Uri notiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notiSound);
        mBuilder.setVibrate(new long[] {50, 1000});
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(5, mBuilder.build());
    }

    public void displayInfo() {
        final Dialog infoDialog = new Dialog(SingaporeQuestionFive.this);
        infoDialog.setContentView(R.layout.infosingaporeq5);
        infoDialog.setTitle("The Food Trail is actually located at Singapore Flyer!");
        infoDialog.show();
        infoDialog.setCancelable(false);
        infoDialog.setCanceledOnTouchOutside(false);
        Button dialogButton = (Button) infoDialog.findViewById(R.id.btnCheck);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ScorePage = new Intent(getApplicationContext(), SingaporeScore.class);
                ScorePage.putExtra("FinalScore", yourCurrentScore);
                ScorePage.putExtra("Name", name);
                startActivity(ScorePage);
                infoDialog.dismiss();
                playFinalScoreNotification();
                countDownTimer = null;
                finish();
            }
        });
    }

    public void playFinalScoreNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.clipboard);
        mBuilder.setContentTitle("You have successfully completed the quiz!");
        mBuilder.setContentText("Your score is " + yourCurrentScore + " / 10 !");
        Uri notiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notiSound);
        mBuilder.setVibrate(new long[] {50, 1000});
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(6, mBuilder.build());
    }
}
