package com.example.user.dcq;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by User on 12/5/2017.
 */

public class HighScoreSingapore extends AppCompatActivity {
    private FloatingActionButton btnBack;
    private MediaPlayer backgroundMusic;
    private SweetAlertDialog warningQuit;
    private TextView txtHighScores;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.highscoresingapore);
        btnBack = (FloatingActionButton) findViewById(R.id.btnBack);
        txtHighScores = (TextView) findViewById(R.id.txtHighScores);
        SharedPreferences scorePrefs = getSharedPreferences(SingaporeScore.QUIZ_PREF, 0);
        String[] savedScores = scorePrefs.getString("highScoresSingapore", "").split("\\|");
        StringBuilder scoreBuild = new StringBuilder("");
        for(String score : savedScores) {
            scoreBuild.append(score+"\n");
        }
        txtHighScores.setText(scoreBuild);
        backgroundMusic = MediaPlayer.create(HighScoreSingapore.this, R.raw.hall_of_fame);
        backgroundMusic.setLooping(true);
        backgroundMusic.start();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningBackDialog();
            }
        });
    }

    public void onBackPressed() {
        showWarningBackDialog();
    }


    public void showWarningBackDialog() {
        warningQuit = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningQuit.setTitleText("Are you sure?");
        warningQuit.setContentText("Return to main page?");
        warningQuit.setCancelable(false);
        warningQuit.setCanceledOnTouchOutside(false);
        warningQuit.setConfirmText("YES");
        warningQuit.show();
        warningQuit.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Intent mainPage = new Intent(getApplicationContext(), MainPage.class);
                startActivity(mainPage);
                sweetAlertDialog.dismiss();
                finish();
            }
        });
        warningQuit.setCancelText("NO");
        warningQuit.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backgroundMusic.stop();
    }
}
