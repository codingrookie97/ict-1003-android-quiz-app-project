package com.example.user.dcq;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainPage extends AppCompatActivity {
    private SweetAlertDialog dialog;
    private Button btnBegin, btnQuit;
    private MediaPlayer backgroundMusic;
    private String getUsername, getQuizType;
    private TextView txtUsername, txtQuizType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        getUsername = prefs.getString("username_preference", "");
        getQuizType = prefs.getString("quiz_preference", "");
        btnBegin = (Button) findViewById(R.id.btnBegin);
        btnQuit = (Button) findViewById(R.id.btnQuit);
        txtUsername = (TextView) findViewById(R.id.txtUsername);
        txtUsername.setText("Username: " + getUsername);
        txtQuizType = (TextView) findViewById(R.id.txtTypeOfQuiz);
        txtQuizType.setText("Quiz Type: " + getQuizType);
        backgroundMusic = MediaPlayer.create(MainPage.this, R.raw.backgroundmusic);
        backgroundMusic.setLooping(true);
        backgroundMusic.start();
        btnBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getQuizType.equals("Singapore Quiz")) {
                    showProgressSingaporeDialog();
                }
                else if (getQuizType.equals("Guess the Country Quiz")) {
                    showProgressCountriesDialog();
                }
            }
        });
        btnQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backgroundMusic.stop();
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_layout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.menu_settings) {
            goToSettingsPage();
        }
        else if (item.getItemId() == R.id.highscores_singapore) {
            goToHighScoresSingaporePage();
        }
        else if (item.getItemId() == R.id.highscores_countries) {
            goToHighScoresCountriesPage();
        }
        return true;
    }

    public void goToSettingsPage() {
        Intent SettingsActivity = new Intent(getApplicationContext(), Settings.class);
        startActivity(SettingsActivity);
        finish();
    }

    public void goToHighScoresSingaporePage() {
        Intent HighScoresSingaporeActivity = new Intent(getApplicationContext(), HighScoreSingapore.class);
        startActivity(HighScoresSingaporeActivity);
        finish();
    }

    public void goToHighScoresCountriesPage() {
        Intent HighScoresCountriesActivity = new Intent(getApplicationContext(), HighScoreCountries.class);
        startActivity(HighScoresCountriesActivity);
        finish();
    }

    public void showProgressSingaporeDialog() {
        dialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitleText("Singapore Quiz");
        dialog.setContentText("Loading questions, please wait a while!");
        dialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent Q1Activity = new Intent(getApplicationContext(), SingaporeQuestionOne.class);
                Q1Activity.putExtra("Name", getUsername);
                startActivity(Q1Activity);
                dialog.dismiss();
                backgroundMusic.stop();
                finish();
            }
        }, 1000);
    }

    public void showProgressCountriesDialog() {
        dialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitleText("Guess the Country Quiz");
        dialog.setContentText("Loading questions, please wait a while!");
        dialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent Q1Activity = new Intent(getApplicationContext(), CountriesQuestionOne.class);
                Q1Activity.putExtra("Name", getUsername);
                startActivity(Q1Activity);
                dialog.dismiss();
                backgroundMusic.stop();
                finish();
            }
        }, 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        backgroundMusic.stop();
    }
}
