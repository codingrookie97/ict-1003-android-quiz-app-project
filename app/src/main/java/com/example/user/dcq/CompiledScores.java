package com.example.user.dcq;

/**
 * Created by User on 12/5/2017.
 */

public class CompiledScores implements Comparable<CompiledScores> {
    private String name;
    public int score;
    private String dateTime;
    @Override
    public int compareTo(CompiledScores compiledScores) {
        return compiledScores.score > score ? 1 : compiledScores.score < score ? -1 : 0;
    }

    public CompiledScores (String getName, int getScore, String getDateTime){
        name = getName;
        score = getScore;
        dateTime = getDateTime;
    }

    public String getScoreText()
    {
        return name + " - " + score + " - " + dateTime;
    }
}
