package com.example.user.dcq;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Settings extends PreferenceActivity {
    private EditTextPreference editUsername;
    private ListPreference listPreference;
    private SweetAlertDialog errorDialog, confirmDialog, progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
        editUsername = (EditTextPreference) findPreference("username_preference");
        listPreference = (ListPreference) findPreference("quiz_preference");
        editUsername.setSummary(editUsername.getText().toString());
        listPreference.setSummary(listPreference.getEntry());
        editUsername.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.toString().equals("")) {
                    showError();
                    return false;
                }
                else {
                    editUsername.setSummary(newValue.toString());
                    return true;
                }
            }
        });
        listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String quizType = (String) newValue;
                CharSequence[] listEntries = listPreference.getEntries();
                int indexValue = listPreference.findIndexOfValue(quizType);
                listPreference.setSummary(listEntries[indexValue]);
                return true;
            }
        });
    }

    public void onBackPressed() {
        displayConfirmSettings();
    }

    public void showError() {
        errorDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
        errorDialog.setTitleText("Please key in your username");
        errorDialog.setContentText("Field must not be blank");
        errorDialog.setCancelable(false);
        errorDialog.setCanceledOnTouchOutside(false);
        errorDialog.setConfirmText("YES");
        errorDialog.show();
        errorDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
            }
        });
    }

    public void displayConfirmSettings() {
        confirmDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        confirmDialog.setTitleText("Are you sure?");
        confirmDialog.setContentText("Confirm your selections?");
        confirmDialog.setCancelable(false);
        confirmDialog.setCanceledOnTouchOutside(false);
        confirmDialog.setConfirmText("YES");
        confirmDialog.setCancelText("NO");
        confirmDialog.show();
        confirmDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                showProgressDialog();
                sweetAlertDialog.dismiss();
            }
        });
        confirmDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
            }
        });
    }

    public void showProgressDialog() {
        progressDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setTitleText("Loading main page");
        progressDialog.setContentText("Please wait!");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backToMainPage();
            }
        }, 500);
    }

    public void backToMainPage() {
        Intent MainPageActivity = new Intent(getApplicationContext(), MainPage.class);
        startActivity(MainPageActivity);
        finish();
    }

}
