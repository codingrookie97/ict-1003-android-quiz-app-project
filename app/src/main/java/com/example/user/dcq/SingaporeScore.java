package com.example.user.dcq;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by User on 7/5/2017.
 */

public class SingaporeScore extends AppCompatActivity {
    private FloatingActionButton btnBack;
    private int finalScore;
    private NotificationManager manager;
    private String name, dateTime;
    private TextView txtFinalScoreTitle, txtFinalScore, txtComments, txtCurrentTime;
    private SharedPreferences quizPreferences;
    public static final String QUIZ_PREF = "ScoreFile";
    private SweetAlertDialog warningQuit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score);
        btnBack = (FloatingActionButton) findViewById(R.id.btnBack);
        txtFinalScoreTitle = (TextView) findViewById(R.id.txtFinalScoreTitle);
        txtFinalScore = (TextView) findViewById(R.id.txtFinalScore);
        txtComments = (TextView) findViewById(R.id.txtComments);
        txtCurrentTime = (TextView) findViewById(R.id.txtCurrentTime);
        quizPreferences = getSharedPreferences(QUIZ_PREF, 0);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        dateTime = dateFormat.format(calendar.getTime());
        txtCurrentTime.setText("You have completed the quiz on " + dateTime + " !" +
                "");
        Intent intent = getIntent();
        finalScore = intent.getExtras().getInt("FinalScore");
        name = intent.getExtras().getString("Name");
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWarningBackDialog();
            }
        });
        txtFinalScoreTitle.setText(name + "'s Final Score:");
        txtFinalScore.setText(finalScore + " / 10");

        if (finalScore == 0) {
            txtComments.setTextColor(Color.parseColor("#FF0000"));
            txtComments.setText("You should study more about Singapore facts!");
        }

        else if (finalScore == 1 || finalScore == 2) {
            txtComments.setTextColor(Color.parseColor("#FF4500"));
            txtComments.setText("You should try harder!");
        }

        else if (finalScore == 3 || finalScore == 4 || finalScore == 5) {
            txtComments.setTextColor(Color.parseColor("#FFA500"));
            txtComments.setText("Good try, but can be improved!");
        }

        else if (finalScore == 6 || finalScore == 7) {
            txtComments.setTextColor(Color.parseColor("#CCCC00"));
            txtComments.setText("You are getting there! Try to improve a bit more!");
        }

        else if (finalScore == 8 || finalScore == 9) {
            txtComments.setTextColor(Color.parseColor("#89E894"));
            txtComments.setText("You are pretty good at this quiz! Try to get full marks next time!");
        }

        else if (finalScore == 10) {
            txtComments.setTextColor(Color.parseColor("#339933"));
            txtComments.setText("Well done, full marks! So easy right!");
        }
        setHighScore();
    }
    public void showWarningBackDialog() {
        warningQuit = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
        warningQuit.setTitleText("Return to main page?");
        warningQuit.setContentText("Are you sure? Thank you for participating in this quiz!");
        warningQuit.setCancelable(false);
        warningQuit.setCanceledOnTouchOutside(false);
        warningQuit.setConfirmText("YES");
        warningQuit.show();
        warningQuit.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Intent mainPage = new Intent(getApplicationContext(), MainPage.class);
                startActivity(mainPage);
                sweetAlertDialog.dismiss();
                manager.cancelAll();
                finish();
            }
        });
        warningQuit.setCancelText("NO");
        warningQuit.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        });
    }

    private void setHighScore() {
        List<CompiledScores> scoresList = new ArrayList<CompiledScores>();
        if (finalScore > 0) {
            SharedPreferences.Editor scoreEdit = quizPreferences.edit();
            String scores = quizPreferences.getString("highScoresSingapore", "");
            if (scores.length() > 0) {
                String[] scoresArray = scores.split("\\|");
                for(String eSc : scoresArray) {
                    String[] parts = eSc.split(" - ");
                    scoresList.add(new CompiledScores(parts[0], Integer.parseInt(parts[1]), parts[2]));
                }
                CompiledScores newScore = new CompiledScores(name, finalScore, dateTime);
                scoresList.add(newScore);
                Collections.sort(scoresList);
                StringBuilder scoreBuild = new StringBuilder("");
                for(int s = 0; s < scoresList.size(); s++) {
                    if(s >= 10) break;
                    if(s > 0) scoreBuild.append("|");
                    scoreBuild.append(scoresList.get(s).getScoreText());
                }
                scoreEdit.putString("highScoresSingapore", scoreBuild.toString());
                scoreEdit.commit();
            }
            else {
                scoreEdit.putString("highScoresSingapore", "" + name + " - " + finalScore + " - " + dateTime);
                scoreEdit.commit();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.cancelAll();
    }
}
